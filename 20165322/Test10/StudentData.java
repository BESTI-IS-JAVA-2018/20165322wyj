import java.util.*;
public class StudentData {
    public  static void main(String[] args) {
        List<Student> list = new LinkedList<>();
        list.add(new Student(20165320,"申启","m",21,66,88,89));
        list.add(new Student(20165321,"谢沛良","m",21,77,99,76));
        list.add(new Student(20165322,"王瑶佳","w",20,88,77,78));
        list.add(new Student(20165323,"杨金川","m",21,99,87,90));
        list.add(new Student(20165324,"何春江","m",20,98,78,93));
        SortByTotal_score sortBytotal_score = new SortByTotal_score();
        Collections.sort(list, sortBytotal_score);
        SortByID sortByID = new SortByID();
        Collections.sort(list, sortByID);
        System.out.println("根据学生学号由低到高排序：");
        for (Student student : list) {
            System.out.println(student);
        }

        Collections.sort(list, sortBytotal_score);
        System.out.println("根据学生成绩由低到高排序：");
        for (Student student : list) {
            System.out.println(student);
        }
    }
}
class Student {

    private int id;//表示学号
    private String name;//表示姓名
    private int age;//表示年龄
    private String sex;//表示性别
    private double computer_score;//表示计算机课程的成绩
    private double english_score;//表示英语课的成绩
    private double maths_score;//表示数学课的成绩
    private double total_score;// 表示总成绩
    private double ave_score; //表示平均成绩

    @Override
    public String toString() {
        return "Student[name:"+name+",age:"+age+",number:"+id+",total_score"+total_score+"]";
    }

    public Student(int id, String name, String sex, int age, double computer_score, double english_score, double maths_score) {
        this.id = id;
        this.name = name;
        this.sex = sex;
        this.age = age;
        this.computer_score = computer_score;
        this.english_score = english_score;
        this.maths_score = maths_score;
    }

    public String getName() {
        return name;
    }
    public int getId() {
        return id;
    }//获得当前对象的学号，

    public double getComputer_score() {
        return computer_score;
    }//获得当前对象的计算机课程成绩,

    public double getMaths_score() {
        return maths_score;
    }//获得当前对象的数学课程成绩,

    public double getEnglish_score() {
        return english_score;
    }//获得当前对象的英语课程成绩,



    public void setName(String name) {
        this.name = name;
    }
    public void setId(int id) {
        this.id = id;
    }// 设置当前对象的id值,

    public void setComputer_score(double computer_score) {
        this.computer_score = computer_score;
    }//设置当前对象的Computer_score值,

    public void setEnglish_score(double english_score) {
        this.english_score = english_score;
    }//设置当前对象的English_score值,

    public void setMaths_score(double maths_score) {
        this.maths_score = maths_score;
    }//设置当前对象的Maths_score值,

    public double getTotalScore() {
        total_score=computer_score + maths_score + english_score;
        return total_score;
    }// 计算Computer_score, Maths_score 和English_score 三门课的总成绩。

    public double getAveScore() {
        return getTotalScore() / 3;
    }// 计算Computer_score, Maths_score 和English_score 三门课的平均成绩。

}

class SortByID implements Comparator<Student> {

    @Override
    public int compare(Student o1, Student o2) {
        return o1.getId() - o2.getId();
    }

}
class SortByTotal_score implements Comparator<Student> {

    @Override
    public int compare(Student o1, Student o2) {
        return (int)( o1.getTotalScore() - o2.getTotalScore());
    }
}
