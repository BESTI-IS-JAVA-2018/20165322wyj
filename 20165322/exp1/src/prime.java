import java.util.Scanner;
public class prime {
    public static void main(String[] args) {
        System.out.println("please input n:");
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        if(n<1||n>32767){
            System.out.println("Please check your input!");
            System.exit(0);
        }
        System.out.println("1到"+ n +"中是质数的值有：");
        for(int i=2;i<=n;i++){
            int flag = 0;
            for(int j=2;j<i;j++) {
                if (i % j == 0)
                    flag = 1;
            }
            if(flag==0){
                System.out.println(i);
            }
        }
    }
}
