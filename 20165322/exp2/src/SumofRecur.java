public class SumofRecur {
  public static void main(String args[]){
   int tmp[]=new int [args.length];
   for(int i=0;i<args.length;i++){
       tmp[i]=Integer.parseInt(args[i]);
   }
   long sum=0;
   if(tmp[0]<=0){
     System.out.println("wrong!");
     System.exit(0);
   }
   for(int i=1;i<=tmp[0];i++){
       sum += fact(i);
   }
    System.out.println(sum);
  }
   public static long fact(int i) {
   if(i==1)
   return 1;
   else 
   return i * fact(i-1);
   }
}
