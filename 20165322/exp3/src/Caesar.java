/**
 * @Date 2018/4/28
 * @author wangyaojia
 */
public class Caesar{
    public static void main(String []args) throws Exception{
        String s=args[0];
        int key=Integer.parseInt(args[1]);
        String es="";
        for(int i=0;i<s.length( );i++)
        {  char c=s.charAt(i);
            // 是小写字母
            c = getC(key, c);
            es+=c;
        }
        System.out.println(es);
    }

    private static char getC(int key, char c) {
        if(c>='a' && c<='z') {
            //移动key%26位
            c = move(key, c);
            if(c<'a') {
                //向左超界
                c = changeCplus(c);
            }
            if(c>'z') {
                //向右超界
                c = changeCminus(c);
            }
        }
        // 是大写字母
        else if(c>='A' && c<='Z') {
            c = move(key, c);
            if(c<'A') {
                c = changeCplus(c);
            }
            if(c>'Z') {
                c = changeCminus(c);
            }
        }
        return c;
    }

    private static char changeCminus(char c) {
        c -= 26;
        return c;
    }

    private static char changeCplus(char c) {
        c += 26;
        return c;
    }

    private static char move(int key, char c) {
        c+=key%26;
        return c;
    }
}
