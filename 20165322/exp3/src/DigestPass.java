import java.security.*;
/**
 * Demo class
 *
 * @author zyx
 * @date 2018/04/28
 */
public class DigestPass{
    public static void main(String[ ] args) throws Exception{
        String x = getString(args[0]);
        MessageDigest m=MessageDigest.getInstance("MD5");
        m.update(x.getBytes("UTF8"));
        byte[] s = getDigest(m);
        String result="";
        for (int i=0; i<s.length; i++){
            result+= getString(s[i]).substring(6);
        }
        System.out.println(result);
    }

    private static String getString(byte b) {
        return Integer.toHexString((0x000000ff & b) |
                0xffffff00);
    }

    private static byte[] getDigest(MessageDigest m) {
        return m.digest();
    }

    private static String getString(String arg) {
        return arg;
    }
}
