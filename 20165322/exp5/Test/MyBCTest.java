import java.util.Scanner;

public class MyBCTest  {
    public static void main (String[] args) {

        String expression1=null,expression2=null, again=null;

        int result,i=0,length=0;

        try
        {
            Scanner in = new Scanner(System.in);

            do
            {
                MyDC evaluator = new MyDC();
                MyBC turner = new MyBC();
                System.out.println ("Enter a valid midfix expression: ");
                expression1 = in.nextLine();
                expression2 = turner.turn(expression1);
                System.out.print ("The new expression is: "+expression2);
                while(expression2.charAt(i)!='\0'){
                    length++;
                    i++;
                }
                expression2 = expression2.substring(1,length-1);
                result = evaluator.evaluate (expression2);
                System.out.println();
                System.out.println ("That expression equals " + result);

                System.out.print ("Evaluate another expression [Y/N]? ");
                again = in.nextLine();
                System.out.println();
            }
            while (again.equalsIgnoreCase("y"));
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }
}
