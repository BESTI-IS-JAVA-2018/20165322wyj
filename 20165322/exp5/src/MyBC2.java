import java.io.IOException;
import java.util.Scanner;


public class MyBC2 {
    public static void main(String[] args)
            throws IOException {
        String expression, again;
        String infix;

        try {
            Scanner in = new Scanner(System.in);
            do {
                System.out.println("Enter a valid infix expression: ");
                expression = in.nextLine();
                MyBC theTrans = new MyBC(expression);
                infix = theTrans.doTrans();
                StringBuilder newInfix = new StringBuilder(infix.replace(" ",""));
                for (int i = 1; i < infix.length()+(i+1)/2 ; i=i+2) {
                    newInfix.insert(i," ");
                }
                System.out.println("Postfix is " + newInfix);
                expression=newInfix.toString();
                System.out.print("Evaluate another expression [Y/N]? ");
                again = in.nextLine();
                System.out.println();
            }
            while (again.equalsIgnoreCase("y"));
        } catch (Exception IOException) {
            System.out.println("Input exception reported");
        }
    }
    private Stack theStack;
    private String input;
    private String output = "";
    public MyBC2(String in) {
        input = in;
        int stackSize = input.length();
        theStack = new Stack(stackSize);
    }
    public String doTrans() {
        for (int j = 0; j < input.length(); j++) {
            char ch = input.charAt(j);
            switch (ch) {
                case '+':
                case '-':
                    gotOper(ch, 1);
                    break;
                case '*':
                case '/':
                    gotOper(ch, 2);
                    break;
                case '(':
                    theStack.push(ch);
                    break;
                case ')':
                    gotParen(ch);
                    break;
                default:
                    output = output + ch;
                    break;
            }
        }
        while (!theStack.isEmpty()) {
            output = output + theStack.pop();
        }
        return output;
    }
    public void gotOper(char opThis, int prec1) {
        while (!theStack.isEmpty()) {
            char opTop = theStack.pop();
            if (opTop == '(') {
                theStack.push(opTop);
                break;
            }
            else {
                int prec2;
                if (opTop == '+' || opTop == '-')
                    prec2 = 1;
                else
                    prec2 = 2;
                if (prec2 < prec1) {
                    theStack.push(opTop);
                    break;
                }
                else
                    output = output + opTop;
            }
        }
        theStack.push(opThis);
    }
    public void gotParen(char ch){
        while (!theStack.isEmpty()) {
            char chx = theStack.pop();
            if (chx == '(')
                break;
            else
                output = output + chx;
        }
    }
    class Stack {
        private int maxSize;
        private char[] stackArray;
        private int top;
        public Stack(int max) {
            maxSize = max;
            stackArray = new char[maxSize];
            top = -1;
        }
        public void push(char j) {
            stackArray[++top] = j;
        }
        public char pop() {
            return stackArray[top--];
        }
        public char peek() {
            return stackArray[top];
        }
        public boolean isEmpty() {
            return (top == -1);
        }
    }
}