import java.util.Random;
public class Fraction {
    private int numerator, denominator;

    public  Fraction (int numer, int denom)
    {

        if(denom == 0 )
            denom = 1;
        if (denom < 0)
        {
            numer = numer * -1;
            denom = denom * -1;
        }
        numerator = numer;
        denominator = denom;

        reduce();
    }

    public int getNumerator()
    {
        return numerator;
    }

    public int getDenominator()
    {
        return denominator;
    }


    public Fraction add(Fraction op2)
    {
        int commonDenominator = denominator * op2.getDenominator();
        int numerator1 = numerator * op2.getDenominator();
        int numerator2 = op2.getNumerator() * denominator;
        int sum = numerator1 + numerator2;
        System.out.print("("+this.toString()+")" + " + " + "("+op2.toString()+")" + "=");
        return new Fraction (sum, commonDenominator);
    }

    public Fraction subtract(Fraction op2)
    {
        int commonDenominator = denominator * op2.getDenominator();
        int numerator1 = numerator * op2.getDenominator();
        int numerator2 = op2.getNumerator() * denominator;
        int difference = numerator1 - numerator2;
        System.out.print("("+this.toString()+")" + " - " + "("+op2.toString()+")" + "=");
        return new Fraction(difference,commonDenominator);
    }

    public Fraction multiply (Fraction op2)
    {
        int numer = numerator * op2.getNumerator();
        int denom = denominator * op2.getDenominator();
        System.out.print("("+this.toString()+")" + " * " + "("+op2.toString()+")" + "=");
        return new Fraction (numer, denom);
    }

    public Fraction divide (Fraction op2)
    {

        int numer = numerator * op2.getDenominator();
        int denom = denominator * op2.getNumerator();
        System.out.print("("+this.toString()+")" + " / " + "("+op2.toString()+")" + "=");
        return new Fraction (numer, denom);
    }

    public String toString()
    {
        String result;

        if (numerator == 0)
            result = "0";
        else
        if(denominator == 0)
            return "错误！分母不能为0";
        else
        if (denominator == 1)
            result = numerator + "";

        else
            result = numerator + "/" + denominator;

        return result;
    }

    private void reduce()
    {
        if (numerator != 0)
        {
            int common = gcd (Math.abs(numerator), denominator);

            numerator = numerator / common;
            denominator = denominator / common;
        }
    }


    private int gcd (int num1, int num2)
    {
        if(num2==0)
            return num1;
        else
            return gcd(num2,num1%num2);

    }
    public static Fraction obj(){
        Random ran = new Random();
        return new Fraction(ran.nextInt(100),ran.nextInt(100));
    }
}
