# 20165322 2017-2018《Java程序设计》第一周学习总结
## 教材学习总结（第一章要点）
- Java的地位及特点 
   - 平台无关 
- 安装JDK
- 开发步骤
   - 编写源文件    
 （1）源文件的名字是某个类的名字，扩展名必须是.java。     
   （2）如果源文件有public类，那么源文件的名字必须是这个类的名字。    
（3）源文件至多有一个public类
   - 编译源文件           
   使用Java编译器（javac.exe）编译源文件，得到字节码文件。    
   C:\ch1> javac Hello.java
   - 运行程序             
   使用Java SE平台中的Java解释器（java.exe）来解释执行字节码文件。    
C:\ch1> java 主类名
- 反编译    
   javap.exe

## 教材中学习的问题及解决
- 安装JDK时，需要在windows更新里勾选针对开发人员再启用Linux bash环境
- git bash打开时出现错误。。
![git](https://gitee.com/uploads/images/2018/0304/203432_6d64e892_1106750.png "FL25A0EX%PPQ$_XZIF7ZC~T.png")
   我在网上没有找到好的解决方案，卸载重装。

## 代码调试中的问题和解决过程
- 我用的是win10的bash，里面不知何原因装不上tree，要注意命令使用时所在的目录。

## [代码托管](https://gitee.com/BESTI-IS-JAVA-2018/20165322wyj.git)
- 代码提交过程截图:
     - 运行 git log --pretty=format:"%h - %an, %cd : %s" 并截图
![daima](https://gitee.com/uploads/images/2018/0304/205622_2de9dbd7_1106750.png "S7TZF8~V8OSE~VSP@1XZO%P.png")
- 代码量截图：
![输入图片说明](https://gitee.com/uploads/images/2018/0304/210000_8aeb02cb_1106750.png "46}(YRF%ZB{C6])V%)S3NAR.png")

## 上周考试错题总结
- 重命名命令为`mv 原名 新命名`
- Linux 第三方软件应该放入/opt目录
- .txt中，对每二列按数字升序排序
- 复制命令：cp A B || cat A > B

## 总结
    刚开始接触新的东西脑子里面还是很迷糊，很多东西都是跟着老师的操作，并不明白其中含义，操作过程中甚至常常不知道自己在干什么ORZ..总之还是应该要靠多加练习来带动理解。