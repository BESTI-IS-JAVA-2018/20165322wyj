class Tom {
   private int x = 120;
   protected int y = 20;
   int z = 11;
   private void f() {
      x = 200;    
      System.out.println(x);
    }
    void g() {
      x = 200;
      System.out.println(x);
    }
}
public class A {
    public static void main(String args[]) {
        Tom tom = new Tom();
        tom.x=22;   //代码1
        tom.y=33;   //代码2
        tom.z=55;   //代码3
        tom.f();    //代码4
        tom.g();    //代码5
     }
}
