class Lader{
  float above;    //梯形的上底
  float bottom;   //梯形的下底
  float height;   //梯形的高
  float area;     //梯形的面积
  float computerArea() {      //定义方法computerArea
    area = (above+bottom)*height/2.0f;
    return area;
  }

  void setHeight(float h) {   //定义方法setHeight
    height = h;
  }
}
