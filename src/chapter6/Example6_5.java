interface SpeakHello{
	void SpeakHello();
}
class Chinese implements SpeakHello{
	public void SpeakHello(){
		System.out.println("中国人习惯问候语：你好，吃饭了吗？");
	}
}
class English implements SpeakHello{
	  public void SpeakHello(){
		  System.out.println("英国人习惯问候语：你好，天气不错");
	  }
}
class KindHello{	
      	public void lookHello(SpeakHello hello){
		hello.SpeakHello();
	}
}
public class Example6_5{
	  public static void main(String[] args) {
		  KindHello kindhello=new KindHello();
		  kindhello.lookHello(new Chinese());
		  kindhello.lookHello(new English());
	  }
}
